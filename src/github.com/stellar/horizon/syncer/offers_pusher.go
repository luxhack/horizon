package syncer

import (
	"github.com/go-errors/errors"
	sbuild "github.com/stellar/go/build"
	sxdr "github.com/stellar/go/xdr"
	"github.com/stellar/horizon/core"
	"github.com/stellar/horizon/db2/history"
	"github.com/stellar/horizon/log"
	"gitlab.com/luxhack/go/amount"
	"gitlab.com/luxhack/go/xdr"
)

type OffersPusher struct {
	q                *history.Q
	offersPuller     *OffersPuller
	gateway          *core.Account
	envelopeListener chan *history.OfferToPush
	accountLoader    core.AccountLoaderI
	txManager        *core.TxManager
	log              *log.Entry
}

func NewOffersPusher(offersPuller *OffersPuller, q *history.Q, gateway *core.Account, accountLoader core.AccountLoaderI, txManager *core.TxManager) *OffersPusher {
	return &OffersPusher{
		q:                q,
		offersPuller:     offersPuller,
		gateway:          gateway,
		accountLoader:    accountLoader,
		txManager:        txManager,
		envelopeListener: make(chan *history.OfferToPush, 100),
		log:              log.WithField("service", "offer_pusher"),
	}
}

func (p *OffersPusher) CheckValid(envelope string) (*xdr.Transaction, error) {
	var txEnvelope xdr.TransactionEnvelope
	err := xdr.SafeUnmarshalBase64(envelope, &txEnvelope)
	if err != nil {
		log.WithStack(err).WithError(err).Error("Failed to unmarshal envelope")
		return nil, err
	}

	ops := txEnvelope.Tx.Operations
	if len(ops) > 1 {
		return nil, nil
	}

	if ops[0].Body.Type != xdr.OperationTypeManageOffer {
		return nil, nil
	}

	if txEnvelope.Tx.Memo.Type != xdr.MemoTypeMemoHash {
		return nil, errors.New("Invalid memo type")
	}

	buyingIssuer := ops[0].Body.ManageOfferOp.Buying.MustAlphaNum4().Issuer
	sellingIssuer := ops[0].Body.ManageOfferOp.Selling.MustAlphaNum4().Issuer
	if buyingIssuer.Address() == sellingIssuer.Address() {
		return nil, errors.New("issuers must be different")
	}

	return &txEnvelope.Tx, nil
}

func (p *OffersPusher) Start() {
	go p.pushOffers()
}

func (p *OffersPusher) Push(offerToPush *history.OfferToPush) {
	if offerToPush == nil || offerToPush.Tx == nil {
		return
	}

	p.envelopeListener <- offerToPush
}

func (p *OffersPusher) pushOffers() {
	for {
		tx := <-p.envelopeListener
		err := p.pushOffer(tx)
		if err != nil {
			p.log.WithError(err).Error("Failed to push offer")
		}
	}
}

func (p *OffersPusher) pushOffer(offerToPush *history.OfferToPush) error {
	op := offerToPush.Tx.Operations[0].Body.MustManageOfferOp()
	selling, err := p.assetToSXDR(op.Selling)
	if err != nil {
		p.log.WithError(err).Error("Failed to convert selling asset to stellar xdr")
		return err
	}
	buying, err := p.assetToSXDR(op.Buying)
	if err != nil {
		p.log.WithError(err).Error("Failed to convert buying asset to stellar xdr")
		return err
	}

	offer := sbuild.ManageOffer(
		false,
		sbuild.Amount(amount.String(op.Amount)),
		sbuild.OfferID(0),
		sbuild.Rate{
			Selling: selling,
			Buying:  buying,
			Price:   sbuild.Price(op.Price.String()),
		},
	)

	err = p.accountLoader.InitSequence(p.gateway)
	if err != nil {
		p.log.WithError(err).Error("Failed to init equence")
		return err
	}

	txToPush := sbuild.Transaction(
		offer,
		sbuild.Network{p.txManager.NetworkID},
		sbuild.SourceAccount{p.gateway.Address()},
		sbuild.Sequence{p.gateway.IncrementSequence()},
	)

	txEnvelopeToPush := txToPush.Sign(p.gateway.Seed())
	if txEnvelopeToPush.Err != nil {
		p.log.WithError(txEnvelopeToPush.Err).Error("Failed to sign tx")
		return txEnvelopeToPush.Err
	}

	rawTxEnvelope, err := sxdr.MarshalBase64(txEnvelopeToPush.E)
	if err != nil {
		p.log.WithError(err).Error("Failed to marshal tx")
	}

	response, err := p.txManager.SubmitStr(rawTxEnvelope)
	if err != nil {
		p.log.WithError(err).Error("Failed to submit tx")
		return err
	}

	if !response.IsResponseSuccess() {
		err = response.ToError()
		p.log.WithError(err).Error("Pushed tx failed")
		return err
	}

	var publicResult sxdr.TransactionResult
	err = sxdr.SafeUnmarshalBase64(*response.ResultXdr, &publicResult)
	if err != nil {
		p.log.WithError(err).Error("Failed to unmarshal public results")
		return err
	}

	offerToPush.PublicResult = &publicResult
	offerToPush.PublicTx = &txEnvelopeToPush.E.Tx

	return p.insertPushedOffer(offerToPush)
}

func (p *OffersPusher) getPublicOfferID(offerToPush *history.OfferToPush) (*int64, error) {
	results := *offerToPush.PublicResult.Result.Results
	offerResult := results[0].Tr.ManageOfferResult.Success.Offer
	if offerResult.Effect == sxdr.ManageOfferEffectManageOfferDeleted {
		return nil, p.offersPuller.Handle(offerToPush)
	}

	id := int64(uint64(offerResult.Offer.OfferId))
	return &id, nil
}

func (p *OffersPusher) getPrivateOfferID(push *history.OfferToPush) int64 {
	results := *push.Result.Result.Results
	return int64(uint64(results[0].Tr.ManageOfferResult.Success.Offer.Offer.OfferId))
}

func (p *OffersPusher) insertPushedOffer(offerToPush *history.OfferToPush) error {
	publicOfferID, err := p.getPublicOfferID(offerToPush)
	if err != nil {
		p.log.WithError(err).Error("Failed to get public offer id")
		return err
	}

	// no need to store - already processed
	if publicOfferID == nil {
		return nil
	}

	privateOfferID := p.getPrivateOfferID(offerToPush)
	return p.q.InsertPushedOffer(history.NewPushedOffer(*publicOfferID, privateOfferID))
}

func (p *OffersPusher) assetToSXDR(asset xdr.Asset) (sbuild.Asset, error) {
	var assetType xdr.AssetType
	var code, issuer string
	err := asset.Extract(&assetType, &code, &issuer)
	if err != nil {
		p.log.WithError(err).Error("Failed to exctract asset")
		return sbuild.Asset{}, err
	}
	return sbuild.CreditAsset(code, issuer), nil
}
