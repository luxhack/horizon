package syncer

import (
	"github.com/stellar/go/xdr"
	"github.com/stellar/horizon/assets"
	"github.com/stellar/horizon/core"
	dbCore "github.com/stellar/horizon/db2/core"
	"github.com/stellar/horizon/db2/history"
	"github.com/stellar/horizon/log"
	sxdr "gitlab.com/luxhack/go/xdr"
	"time"
)

type OffersPuller struct {
	gateway         *core.Account
	q               *history.Q
	coreQ           *dbCore.Q
	publicTxManager *core.TxManager
	publicTxCreator *core.TxCreator

	log *log.Entry
}

func NewOfferPuller(gateway *core.Account, coreQ *dbCore.Q, q *history.Q, publicTxManager *core.TxManager, privateTxCreator *core.TxCreator) *OffersPuller {
	return &OffersPuller{
		q:               q,
		coreQ:           coreQ,
		gateway:         gateway,
		publicTxManager: publicTxManager,
		publicTxCreator: privateTxCreator,
		log:             log.WithField("serivce", "offers_puller"),
	}
}

func (p *OffersPuller) Handle(pushedOffer *history.OfferToPush) error {
	// handle offer taken by ours
	return nil
}

func (p *OffersPuller) Start() {
	go p.pull()
}

func (p *OffersPuller) pull() {
	pagingToken, err := p.q.GetLatestProcessedTx()
	if err != nil {
		p.log.WithError(err).Panic("Failed to get latest processed tx")
	}
	for {
		// need to sleep to not exceed rate limits
		time.Sleep(time.Duration(5)*time.Second)
		pagingToken, err = p.processFrom(pagingToken)
		if err != nil {
			p.log.WithError(err).Error("Failed process txs")
			pagingToken = 0
		}
	}
}

func (p *OffersPuller) processFrom(pagingToken int64) (int64, error) {
	txs, err := p.publicTxManager.GetTxs(p.gateway.Address(), pagingToken)
	if err != nil {
		p.log.WithError(err).Error("Failed to get txs")
		return 0, err
	}

	if !txs.IsResponseStatusSuccess() {
		err = txs.ToError()
		p.log.WithError(err).Error("Failed to get txs")
		return 0, err
	}

	for i := range txs.Embedded.Txs {
		err = p.processTx(&txs.Embedded.Txs[i])
		if err != nil {
			p.log.WithError(err).Error("Failed to process tx")
			return 0, err
		}

		pagingToken = txs.Embedded.Txs[0].GetPagingToken()
	}

	return pagingToken, nil
}

func (p *OffersPuller) processTx(tx *core.Tx) error {
	var txEnv sxdr.TransactionEnvelope
	err := sxdr.SafeUnmarshalBase64(tx.EnvelopeXdr, &txEnv)
	if err != nil {
		p.log.WithError(err).Error("Failed to unmarshal tx")
		return err
	}

	if len(txEnv.Tx.Operations) > 1 {
		return nil
	}

	// outgoing txs already processed
	if txEnv.Tx.SourceAccount.Address() == p.gateway.Address() {
		return nil
	}

	if txEnv.Tx.Operations[0].Body.Type != sxdr.OperationTypeManageOffer {
		return nil
	}

	if txEnv.Tx.Memo.Type != sxdr.MemoTypeMemoHash {
		return nil
	}

	receiverPublicKey := [32]byte(*txEnv.Tx.Memo.Hash)
	receiverAccountID, err := xdr.NewAccountId(xdr.PublicKeyTypePublicKeyTypeEd25519, xdr.Uint256(receiverPublicKey))
	if err != nil {
		p.log.WithError(err).Error("Failed to get public key")
		// ignore error
		return nil
	}

	var result sxdr.TransactionResult
	err = sxdr.SafeUnmarshalBase64(tx.ResultXdr, &result)
	if err != nil {
		p.log.WithError(err).Error("Failed to unmarshal result")
		return err
	}

	results := *result.Result.Results
	offers := results[0].Tr.ManageOfferResult.Success.OffersClaimed
	for i := range offers {
		err = p.processOffer(receiverAccountID, &offers[i])
		if err != nil {
			p.log.WithError(err).Error("Failed to process taken offer")
			return err
		}
	}

	return nil
}

func (p *OffersPuller) processOffer(receiver xdr.AccountId, offer *sxdr.ClaimOfferAtom) error {
	pushedOffer, err := p.q.PushedOfferByPublicOfferID(int64(uint64(offer.OfferId)))
	if err != nil {
		p.log.WithError(err).Error("Failed to get pushed offer by public id")
		return err
	}

	if pushedOffer == nil {
		return nil
	}

	privateOffer, err := p.coreQ.OffersByID(pushedOffer.PrivateOfferID)
	if err != nil {
		p.log.WithError(err).Error("Failed to get private offer by id")
		return err
	}

	if privateOffer == nil {
		return nil
	}

	var assetType, assetIssuer, assetCode string
	err = offer.AssetSold.Extract(&assetType, &assetCode, &assetIssuer)
	if err != nil {
		p.log.WithError(err).Error("Failed to get asset data")
		return err
	}

	asset, err := assets.CreateAlphaNumAsset(assetCode, assetIssuer)
	if err != nil {
		p.log.WithError(err).Error("Failed to create asset")
		return err
	}
	err = p.publicTxCreator.SubmitMatchOffer(uint64(pushedOffer.PrivateOfferID), offer.SellerId.Address(), receiver.Address(), int64(offer.AmountSold), asset)
	if err != nil {
		p.log.WithError(err).Error("Failed to submit matching offer")
		return err
	}

	return nil
}
