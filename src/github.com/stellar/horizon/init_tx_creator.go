package horizon

import (
	"github.com/stellar/horizon/core"
	"gitlab.com/luxhack/go/keypair"
	"github.com/stellar/horizon/syncer"
)

func initTxCreator(app *App) {
	if app.config.GatewaySeed == "" {
		panic("invalid gateway seed")
	}

	if app.config.PublicHorizonURL == "" {
		panic("invalid PublicHorizonURL")
	}

	if app.config.HorizonURL == "" {
		panic("invalid HorizonURL")
	}

	if app.config.NetworkID == "" {
		panic("invalid NetworkID")
	}

	if app.config.PublicNewtworkID == "" {
		panic("invalid PublicNewtworkID")
	}

	txManager := core.NewTxManager(app.config.HorizonURL, app.config.NetworkID)
	accountLoader := core.NewAccountLoader(txManager)
	app.txCreator = core.NewTxCreator(app.config.GatewaySeed, txManager, accountLoader)

	publicTxManager := core.NewTxManager(app.config.PublicHorizonURL, app.config.PublicNewtworkID)
	publicAccountLoader := core.NewAccountLoader(publicTxManager)
	gateway := keypair.MustParse(app.config.GatewaySeed).(*keypair.Full)
	gatewayAccount := core.NewAccount(gateway, 0)


	app.offersPuller = syncer.NewOfferPuller(gatewayAccount, app.coreQ, app.HistoryQ(), publicTxManager, app.txCreator)
	app.offersPuller.Start()

	app.offersPusher = syncer.NewOffersPusher(app.offersPuller, app.HistoryQ(), gatewayAccount, publicAccountLoader, publicTxManager)
	app.offersPusher.Start()
}

func init() {
	appInit.Add("tx_creator", initTxCreator, "horizon-db", "core-db", "app-context", "log")
}
