package horizon

// OrderBookShowAction renders a account summary found by its address.
type OrderBookShowAction struct {
	Action
}

// JSON is a method for actions.JSON
func (action *OrderBookShowAction) JSON() {

	action.Do(func() {
		action.RedirectToPublic()
	})
}
