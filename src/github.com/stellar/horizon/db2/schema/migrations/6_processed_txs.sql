-- +migrate Up
CREATE TABLE processed_txs (
  paging_token BIGINT NOT NULL,
  PRIMARY KEY (paging_token)
);

-- +migrate Down
DROP TABLE IF EXISTS processed_txs;
