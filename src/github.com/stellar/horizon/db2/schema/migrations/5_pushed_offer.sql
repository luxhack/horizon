-- +migrate Up
CREATE TABLE pushed_offer (
  public_offer_id BIGINT NOT NULL,
  private_offer_id BIGINT NOT NULL,
  PRIMARY KEY (public_offer_id)
);

-- +migrate Down
DROP TABLE IF EXISTS pushed_offer;
