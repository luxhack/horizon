package history

import (
	sq "github.com/Masterminds/squirrel"
)

type ProcessedTx struct {
	PagingToken int64 `db:"paging_token"`
}

func (q *Q) InsertProcessedTx(pagingToken int64) error {
	sql := sq.Insert("processed_txs").Columns("paging_token").Values(pagingToken)
	_, err := q.Exec(sql)
	return err
}

func (q *Q) GetLatestProcessedTx() (int64, error) {
	sql := selectProcessedTx.Limit(1).OrderBy("p.paging_token DESC")
	var pushedOffer ProcessedTx
	err := q.Get(&pushedOffer, sql)
	if err != nil {
		if q.NoRows(err) {
			return 0, nil
		}
		return 0, err
	}

	return pushedOffer.PagingToken, nil
}

// AccountByID loads a row from `history_accounts`, by id
func (q *Q) Exists(processedTx int64) (bool, error) {
	sql := selectProcessedTx.Limit(1).Where("p.paging_token = ?", processedTx)
	var pushedOffer ProcessedTx
	err := q.Get(&pushedOffer, sql)
	if err != nil {
		if q.NoRows(err) {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

var selectProcessedTx = sq.Select("p.*").From("processed_txs p")



