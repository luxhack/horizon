package history

import (
	sq "github.com/Masterminds/squirrel"
	sxdr "github.com/stellar/go/xdr"
	"gitlab.com/luxhack/go/xdr"
)

type OfferToPush struct {
	Tx           *xdr.Transaction
	Result       *xdr.TransactionResult

	PublicTx     *sxdr.Transaction
	PublicResult *sxdr.TransactionResult
}

type PushedOffer struct {
	PublicOfferID  int64 `db:"public_offer_id"`
	PrivateOfferID int64 `db:"private_offer_id"`
}

func NewPushedOffer(pub, priv int64) *PushedOffer {
	return &PushedOffer{
		PublicOfferID:  pub,
		PrivateOfferID: priv,
	}
}

func (q *Q) InsertPushedOffer(p *PushedOffer) error {
	sql := sq.Insert("pushed_offer").Columns("public_offer_id, private_offer_id").Values(p.PublicOfferID, p.PrivateOfferID)
	_, err := q.Exec(sql)
	return err
}

// AccountByID loads a row from `history_accounts`, by id
func (q *Q) PushedOfferByPublicOfferID(publicOfferID int64) (*PushedOffer, error) {
	sql := selectPushedOffer.Limit(1).Where("p.public_offer_id = ?", publicOfferID)
	var pushedOffer PushedOffer
	err := q.Get(pushedOffer, sql)
	if err != nil {
		if q.NoRows(err) {
			return nil, nil
		}
		return nil, err
	}

	return &pushedOffer, nil
}

var selectPushedOffer = sq.Select("p.*").From("pushed_offer p")
