package core

import (
	"encoding/json"
	"errors"
	"gitlab.com/luxhack/go/xdr"
)

// TxResponse contains result of submitting transaction to Stellar network
type TxResponse struct {
	Response
	Hash      string  `json:"hash,omitempty"`
	ResultXdr *string `json:"result_xdr,omitempty"` // Only success response.
	Ledger    *uint64 `json:"ledger"`
	Extras    *struct {
		EnvelopeXdr string      `json:"envelope_xdr"`
		ResultXdr   string      `json:"result_xdr"`
		ResultCodes interface{} `json:"result_codes"`
	} `json:"extras,omitempty"`

	result *xdr.TransactionResult
}

func (response *TxResponse) ToError() error {
	jByte, _ := json.Marshal(response)
	return errors.New(string(jByte))
}

func (response *TxResponse) IsResponseSuccess() bool {
	return response.IsResponseStatusSuccess() && response.Ledger != nil
}
