package core

import (
	"github.com/stellar/horizon/log"
	"gitlab.com/luxhack/go/keypair"
	"gitlab.com/luxhack/go/xdr"
)

type TxCreator struct {
	accountLoader AccountLoaderI
	txManager     *TxManager
	gateway       *Account
	log           *log.Entry
}

func NewTxCreator(gatewaySeed string, txManager *TxManager, accountLoader AccountLoaderI) *TxCreator {
	gateway := keypair.MustParse(gatewaySeed).(*keypair.Full)
	return &TxCreator{
		txManager:     txManager,
		accountLoader: accountLoader,
		gateway:       NewAccount(gateway, 0),
		log:           log.WithField("service", "tx_creator"),
	}
}

func (c *TxCreator) CreateAccount(accountID string) error {
	createAccountOp := xdr.CreateAccountOp{}
	err := createAccountOp.Destination.SetAddress(accountID)
	if err != nil {
		c.log.WithError(err).Error("Failed to set address")
		return err
	}

	op := xdr.Operation{
		Body: xdr.OperationBody{
			Type:            xdr.OperationTypeCreateAccount,
			CreateAccountOp: &createAccountOp,
		},
	}

	return c.SubmitOperation(op)
}

func (c *TxCreator) SubmitMatchOffer(offerID uint64, from, to string, amount int64, asset xdr.Asset) error {
	matchOffer := xdr.MatchOffer{
		OfferId: xdr.Uint64(offerID),
		Asset:   asset,
		Amount:  xdr.Int64(amount),
	}

	matchOffer.From.SetAddress(from)
	matchOffer.To.SetAddress(to)
	op := xdr.Operation{
		Body: xdr.OperationBody{
			Type:       xdr.OperationTypeOfferMatch,
			MatchOffer: &matchOffer,
		},
	}

	return c.SubmitOperation(op)
}

func (c *TxCreator) SubmitTransaction(source *Account, op xdr.Operation) error {
	return c.SubmitTransactionWithMemo(source, op, "")
}

func (c *TxCreator) SubmitTransactionWithMemo(source *Account, op xdr.Operation, memo string) error {
	err := c.accountLoader.InitSequence(source)
	if err != nil {
		c.log.WithError(err).Error("Failed to init seq")
		return err
	}

	tx, _, err := c.txManager.SignWithMemo(source, source, []xdr.Operation{op}, c.txManager.NetworkID, memo)
	if err != nil {
		return err
	}

	resp, err := c.txManager.SubmitRaw(tx)
	if err != nil {
		c.log.WithError(err).Error("Failed to submit")
		return err
	}

	if !resp.IsResponseStatusSuccess() {
		source.SetSequence(0)
		err = resp.ToError()
		if err != nil {
			c.log.WithError(err).Error("Failed to create account")
			return err
		}
	}

	return nil
}

func (c *TxCreator) SubmitOperation(op xdr.Operation) error {
	return c.SubmitTransaction(c.gateway, op)
}
