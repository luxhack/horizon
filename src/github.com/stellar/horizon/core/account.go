package core

import (
	"sync"
	"gitlab.com/luxhack/go/xdr"
	"gitlab.com/luxhack/go/keypair"
	"github.com/stellar/horizon/log"
)

type Account struct {
	kp            *keypair.Full
	sequence      uint64
	sequenceMutex sync.Mutex

	seed      string
	address   string
	accountId *xdr.AccountId
}

func NewAccount(kp *keypair.Full, sequence uint64) *Account {
	return &Account{
		kp:       kp,
		sequence: sequence,
	}
}

func (a *Account) SetSequence(seq uint64) {
	a.sequenceMutex.Lock()
	defer a.sequenceMutex.Unlock()
	if a.sequence == 0 || seq == 0 {
		a.sequence = seq
	}
}

func (a *Account) IncrementSequence() uint64 {
	a.sequenceMutex.Lock()
	defer a.sequenceMutex.Unlock()
	a.sequence++
	return a.sequence
}

func (a *Account) Sequence() uint64 {
	a.sequenceMutex.Lock()
	defer a.sequenceMutex.Unlock()
	return a.sequence
}

func (a *Account) Seed() string {
	if a.seed == "" {
		a.seed = a.kp.Seed()
	}
	return a.seed
}

func (a *Account) Address() string {
	if a.address == "" {
		a.address = a.kp.Address()
	}
	return a.address
}

func (a *Account) AccountId() xdr.AccountId {
	if a.accountId == nil {
		a.accountId = new(xdr.AccountId)
		err := a.accountId.SetAddress(a.Address())
		if err != nil {
			log.Panic("Invalid account address - can't convert to account id")
		}
	}
	return *a.accountId
}

func (a *Account) SignDecorated(data []byte) (xdr.DecoratedSignature, error) {
	return a.kp.SignDecorated(data)
}
