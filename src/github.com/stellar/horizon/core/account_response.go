package core

type AccountTypes struct {
	TypeI uint32 `json:"type_i"`
}

// AccountResponse contains account data returned by Horizon
type AccountResponse struct {
	Response
	AccountID      string       `json:"id"`
	SequenceNumber string       `json:"sequence"`
	Types          AccountTypes `json:"types"`
}
