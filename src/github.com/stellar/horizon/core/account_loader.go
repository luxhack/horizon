package core

import (
	"github.com/go-errors/errors"
	"strconv"
	"github.com/stellar/horizon/log"
)

type AccountLoaderI interface {
	InitSequence(account *Account) error
}

type AccountLoader struct {
	TxManager *TxManager
	log       *log.Entry
}

func NewAccountLoader(txManager *TxManager) *AccountLoader {
	return &AccountLoader{
		TxManager: txManager,
		log:       log.WithField("service", "account_manager"),
	}
}

func (m *AccountLoader) InitSequence(account *Account) error {
	if account.Sequence() == 0 {
		accountResponse, err := m.TxManager.GetAccount(account.Address())
		if err != nil {
			m.log.WithStack(err).WithError(err).Error("Failed to load account.")
			return err
		}

		if accountResponse == nil {
			err := errors.New("Account not found")
			return err
		}

		seq, err := strconv.ParseUint(accountResponse.SequenceNumber, 10, 64)
		if err != nil {
			m.log.WithError(err).Debug("Failed to parse sequence number")
			return err
		}
		account.SetSequence(seq)
	}
	return nil
}
