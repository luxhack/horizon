package core

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/go-errors/errors"
	"github.com/stellar/horizon/httpx"
	"gitlab.com/luxhack/go/hash"
	"gitlab.com/luxhack/go/network"
	"gitlab.com/luxhack/go/xdr"
	"net/url"
	"time"
)

// TxSubmitter implements methods to submit/get txs from horizon
type TxManager struct {
	horizon   *url.URL
	NetworkID string

	httpClient *httpx.JsonHttpClient

	log *logrus.Entry
}

// Creates a new NewTxManager instance.
func NewTxManager(rawHorizon, networkID string) *TxManager {
	log := logrus.WithField("service", "core")

	horizon, err := url.Parse(rawHorizon)
	if err != nil {
		log.WithError(err).Panic("Invalid horizon url")
	}

	return &TxManager{
		NetworkID:  networkID,
		horizon:    horizon,
		log:        log,
		httpClient: httpx.NewJsonHttpClient(time.Duration(30) * time.Second),
	}
}

func (m *TxManager) getURL() url.URL {
	return *m.horizon
}

func (m *TxManager) Submit(v *url.Values) (*TxResponse, error) {
	response := new(TxResponse)
	query := m.getURL()
	query.Path += "/transactions"
	err := m.httpClient.PostForm(query.String(), v, response)
	if err != nil {
		return nil, errors.WrapPrefix(err, "Failed to submit", 0)
	}
	return response, nil
}

func (m *TxManager) GetAccount(address string) (*AccountResponse, error) {
	query := m.getURL()
	query.Path = "/accounts/" + address
	var accountResponse AccountResponse
	err := m.httpClient.Get(query.String(), &accountResponse)
	if err != nil {
		m.log.WithError(err).Debug("Failed to load account")
		return nil, err
	}

	if accountResponse.IsResponseStatusNotFound() {
		return nil, nil
	}

	if !accountResponse.IsResponseStatusSuccess() {
		return nil, errors.New(fmt.Sprintf("Failed to load account. Status: %d", accountResponse.Status))
	}

	return &accountResponse, nil
}

func (m *TxManager) SubmitStr(rawTx string) (*TxResponse, error) {
	v := url.Values{}
	v.Set("tx", rawTx)
	return m.Submit(&v)
}

func (m *TxManager) SubmitRaw(tx *xdr.TransactionEnvelope) (*TxResponse, error) {
	txeBase64, err := xdr.MarshalBase64(tx)
	if err != nil {
		m.log.WithError(err).Error("Failed to marshal tx")
		return nil, err
	}

	return m.SubmitStr(txeBase64)
}

func (m *TxManager) getMemo(rawMemo string) xdr.Memo {
	if rawMemo == "" {
		return xdr.Memo{
			Type: xdr.MemoTypeMemoNone,
		}
	}

	var accountID xdr.AccountId
	accountID.SetAddress(rawMemo)
	memo := xdr.Hash([32]byte(*accountID.Ed25519))
	return xdr.Memo{
		Type: xdr.MemoTypeMemoHash,
		Hash: &memo,
	}
}

func (m *TxManager) SignWithMemo(source, signer *Account, ops []xdr.Operation, networkID string, rawMemo string) (*xdr.TransactionEnvelope, string, error) {
	seq := source.IncrementSequence()
	tx := &xdr.Transaction{
		SourceAccount: source.AccountId(),
		SeqNum:        xdr.SequenceNumber(seq),
		Operations:    ops,
		Memo:          m.getMemo(rawMemo),
	}

	rawHash, err := network.HashTransaction(tx, networkID)
	if err != nil {
		m.log.WithError(err).Error("Failed to get hash of tx")
		return nil, "", err
	}

	txHash := rawHash[:]
	signature, err := signer.SignDecorated(txHash)
	if err != nil {
		m.log.WithError(err).Error("Failed to sign tx")
		return nil, "", err
	}

	return &xdr.TransactionEnvelope{
		Tx:         *tx,
		Signatures: []xdr.DecoratedSignature{signature},
	}, hex.EncodeToString(txHash), nil
}

func (m *TxManager) Sign(source, signer *Account, ops []xdr.Operation, networkID string) (*xdr.TransactionEnvelope, string, error) {
	return m.SignWithMemo(source, signer, ops, networkID, "")
}

func (m *TxManager) hash(tx *xdr.Transaction, networkID string) ([32]byte, error) {
	var txBytes bytes.Buffer

	_, err := fmt.Fprintf(&txBytes, "%s", networkID)
	if err != nil {
		return [32]byte{}, err
	}

	_, err = xdr.Marshal(&txBytes, xdr.EnvelopeTypeEnvelopeTypeTx)
	if err != nil {
		return [32]byte{}, err
	}

	_, err = xdr.Marshal(&txBytes, tx)
	if err != nil {
		return [32]byte{}, err
	}

	return hash.Hash(txBytes.Bytes()), nil
}

func (m *TxManager) GetTxs(account string, from int64) (*TxsResponse, error) {
	query := m.getURL()
	query.Path += fmt.Sprintf("/accounts/%s/transactions", account)
	query.RawQuery = fmt.Sprintf("order=asc&cursor=%d", from)
	var result TxsResponse
	err := m.httpClient.Get(query.String(), &result)
	if err != nil {
		m.log.WithError(err).Error("Failed to get txs")
		return nil, err
	}

	return &result, nil
}
