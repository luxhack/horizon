package core

import (
	"encoding/json"
	"github.com/go-errors/errors"
)

type ResponseStatus int

const (
	RESPONSE_STATUS_SUCCESS                  = 0
	RESPONSE_STATUS_NOT_FOUND ResponseStatus = 404
)

type Response struct {
	Status ResponseStatus `json:"status"`
}

func (r *Response) IsResponseStatusSuccess() bool {
	return r.Status == RESPONSE_STATUS_SUCCESS
}

func (r *Response) IsResponseStatusNotFound() bool {
	return r.Status == RESPONSE_STATUS_NOT_FOUND
}

func (r *Response) ToError() error {
	data, _ := json.Marshal(r)
	return errors.New(string(data))
}
