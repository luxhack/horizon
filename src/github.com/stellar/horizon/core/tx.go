package core

import "strconv"

type Embedded struct {
	Txs []Tx `json:"records"`
}

type TxsResponse struct {
	Response
	Embedded Embedded `json:"_embedded"`
}

type Tx struct {
	PagingToken   string `json:"paging_token"`
	SourceAccount string `json:"source_account"`
	EnvelopeXdr   string `json:"envelope_xdr"`
	ResultXdr     string `json:"result_xdr"`
}

func (t *Tx) GetPagingToken() int64 {
	result, _ := strconv.ParseInt(t.PagingToken, 10, 64)
	return result
}
