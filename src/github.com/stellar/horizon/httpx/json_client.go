package httpx

import (
	"bytes"
	"encoding/json"
	"github.com/Sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type ResponseWithStatus interface {
	SetStatusCode(int)
}

type JsonHttpClient struct {
	client *http.Client
	log    *logrus.Entry

	defaultHeaders map[string]string
}

func NewJsonHttpClientWithHeaders(requestTimeout time.Duration, headers map[string]string) *JsonHttpClient {
	return &JsonHttpClient{
		client: &http.Client{
			Timeout: requestTimeout,
		},
		log:            logrus.WithField("service", "jsonHttpClient"),
		defaultHeaders: headers,
	}
}

func NewJsonHttpClient(requestTimeout time.Duration) *JsonHttpClient {
	return NewJsonHttpClientWithHeaders(requestTimeout, nil)
}

func (c *JsonHttpClient) Get(url string, response interface{}) error {
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	return c.Do(request, response)
}

func (c *JsonHttpClient) GetWithToken(url, token string, response interface{}) error {
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	request.Header.Add("Authorization", "Bearer "+token)
	return c.Do(request, response)
}

func (c *JsonHttpClient) DeleteWithToken(url, token string, response interface{}) error {
	request, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	request.Header.Add("Authorization", "Bearer "+token)
	return c.Do(request, response)
}

func (c *JsonHttpClient) GetWithBasicAuth(url, userName, password string, response interface{}) error {
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	request.SetBasicAuth(userName, password)
	return c.Do(request, response)
}

func (c *JsonHttpClient) PostForm(url string, data *url.Values, response interface{}) error {
	request, err := http.NewRequest("POST", url, strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	return c.Do(request, response)
}

func (c *JsonHttpClient) Patch(url string, response interface{}) error {
	request, err := http.NewRequest("PATCH", url, nil)
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")
	return c.Do(request, response)
}

func (c *JsonHttpClient) createPostRequest(url string, requestBody interface{}) (*http.Request, error) {
	rawRequestBody, err := c.marshalJsonBody(requestBody)
	if err != nil {
		c.log.WithError(err).Error("Failed to marshal request body")
		return nil, err
	}

	request, err := http.NewRequest("POST", url, rawRequestBody)
	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")
	return request, nil
}

func (c *JsonHttpClient) marshalJsonBody(body interface{}) (io.Reader, error) {
	if body == nil {
		return nil, nil
	}

	rawRequestBody, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(rawRequestBody), nil
}

func (c *JsonHttpClient) PostJson(url string, requestBody, response interface{}) error {
	request, err := c.createPostRequest(url, requestBody)
	if err != nil {
		return err
	}

	return c.Do(request, response)
}

func (c *JsonHttpClient) PostJsonWithToken(url, token string, requestBody, response interface{}) error {
	request, err := c.createPostRequest(url, requestBody)
	if err != nil {
		return err
	}

	request.Header.Add("Authorization", "Bearer "+token)
	return c.Do(request, response)
}

func (c *JsonHttpClient) PostBasicAuthJson(url, userName, password string, requestBody, response interface{}) error {
	request, err := c.createPostRequest(url, requestBody)
	if err != nil {
		return err
	}

	request.SetBasicAuth(userName, password)
	return c.Do(request, response)
}

func (c *JsonHttpClient) Do(request *http.Request, response interface{}) error {
	for key, value := range c.defaultHeaders {
		request.Header.Set(key, value)
	}

	statusCode, body, err := c.do(request)
	if err != nil {
		return err
	}

	c.log.WithField("request", request.URL.String()).Debug("Got response")
	c.log.WithField("body", string(body)).Debug("Got response")
	if len(body) == 0 || response == nil {
		c.trySetStatusCode(response, statusCode)
		return nil
	}

	err = json.Unmarshal(body, response)
	if err != nil {
		c.log.WithError(err).WithField("body", string(body)).Error("Failed to unmarshal response")
	}

	c.trySetStatusCode(response, statusCode)

	return err
}

func (c *JsonHttpClient) trySetStatusCode(response interface{}, statusCode int) {
	if response == nil {
		return
	}

	statusSetable, ok := response.(ResponseWithStatus)
	if !ok {
		return
	}

	statusSetable.SetStatusCode(statusCode)
}

func (c *JsonHttpClient) do(request *http.Request) (int, []byte, error) {
	resp, err := c.client.Do(request)
	if err != nil {
		return 0, []byte{}, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	return resp.StatusCode, body, err
}
